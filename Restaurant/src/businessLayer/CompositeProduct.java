package businessLayer;

import java.util.ArrayList;

@SuppressWarnings("serial")
public class CompositeProduct extends MenuItem {

	private ArrayList<MenuItem> itemList = new ArrayList<MenuItem>();
	
	public CompositeProduct(String itemName, MenuItem m) {
		super(itemName);
		itemList.add(m);
	}
	
	public void addMenuItem(MenuItem m) {
		itemList.add(m);
	}
	
	public float computePrice() {
		float price = 0;
		for(MenuItem m : itemList) {
			price += m.computePrice();
		}
		return price * 0.8f + 1;
	}
	
	public String listIngredients() {
		String ret = "Ingredients: ";
		for(MenuItem m : itemList) {
			ret += m.getItemName() + ", ";
		}
		ret = ret.substring(0, ret.length() - 2);
		ret += ".\n";
		return ret;
	}

}
