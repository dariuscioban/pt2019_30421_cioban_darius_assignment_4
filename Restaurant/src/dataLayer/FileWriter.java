package dataLayer;

import java.io.PrintWriter;

public class FileWriter {
	
	String filename = null;
	
	public FileWriter(String filename) {
		this.filename = filename + ".txt";
	}
	
	public void write(String msg) {
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(filename, "UTF-8");
			writer.println(msg);
		} catch (Exception e) {
			
		} finally {
			writer.close();
		}
	}
}
