package businessLayer;

@SuppressWarnings("serial")
public class BaseProduct extends MenuItem{
	
	private float price;
	public BaseProduct(String productName, float price) {
		super(productName);
		this.price = price;
	}
	
	public void setPrice(float price) {
		this.price = price;
	}
	
	public float computePrice() {
		return price;
	}
}
