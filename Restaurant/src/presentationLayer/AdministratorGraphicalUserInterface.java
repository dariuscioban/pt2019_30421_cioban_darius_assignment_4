package presentationLayer;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import businessLayer.*;
import dataLayer.RestaurantSerializator;
import javax.swing.*;

public class AdministratorGraphicalUserInterface {
	
	private static final int ADD_ITEM = 0;
	private static final int EDIT_ITEM = 1;
	private static final int DELETE_ITEM = 2;
	private static final int VIEW_MENU = 3;
	
	RestaurantSerializator rs = new RestaurantSerializator("restaurant");
	Restaurant r;
	
	private JFrame frame;
	JPanel tablePanel = new JPanel();
	JPanel rightPanel = new JPanel();
	JTextField itemNameField = new JTextField(10);
	JTextField itemEditNameField = new JTextField(10);
	JTextField itemPriceField = new JTextField(10);
	JTextField itemIngredientsField = new JTextField(10);
	
	public AdministratorGraphicalUserInterface() {
		r = rs.deserialize();
	}
	
	public void initUI() {
		frame = new JFrame("Administrator");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(1400, 600);
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.X_AXIS));
		JPanel leftPanel = new JPanel();
		leftPanel.setLayout(new BoxLayout(leftPanel, BoxLayout.Y_AXIS));
		rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.Y_AXIS));
		tablePanel.setLayout(new BorderLayout());
		JPanel fieldPanel = new JPanel();
		JPanel buttonPanel = new JPanel();
		
		JButton addItemButton = new JButton("Add");
		addItemButton.addActionListener(new Clicker(ADD_ITEM));
		JButton editItemButton = new JButton("Edit");
		editItemButton.addActionListener(new Clicker(EDIT_ITEM));
		JButton deleteItemButton = new JButton("Delete");
		deleteItemButton.addActionListener(new Clicker(DELETE_ITEM));
		JButton viewMenuButton = new JButton("View");
		viewMenuButton.addActionListener(new Clicker(VIEW_MENU));
		
		fieldPanel.add(new JLabel("Item name: "));
		fieldPanel.add(itemNameField);
		fieldPanel.add(new JLabel("Ingredients : "));
		fieldPanel.add(itemIngredientsField);
		fieldPanel.add(new JLabel("Price : "));
		fieldPanel.add(itemPriceField);
		fieldPanel.add(new JLabel("New Name: "));
		fieldPanel.add(itemEditNameField);
		
		buttonPanel.add(addItemButton);
		buttonPanel.add(editItemButton);
		buttonPanel.add(deleteItemButton);
		buttonPanel.add(viewMenuButton);
		
		leftPanel.add(fieldPanel);
		leftPanel.add(new JSeparator());
		leftPanel.add(buttonPanel);
		rightPanel.add(tablePanel);
		mainPanel.add(leftPanel);
		mainPanel.add(new JSeparator());
		mainPanel.add(rightPanel);
		frame.setContentPane(mainPanel);
		frame.setVisible(true);
	}
	
	private class Clicker implements ActionListener {
		
		int mode;
		
		public Clicker(int mode) {
			this.mode = mode;
		}
		
		public void actionPerformed(ActionEvent arg0) {
			switch (mode) {
				case ADD_ITEM:
					if(itemIngredientsField.getText().equals("")) {
						BaseProduct bp = new BaseProduct(itemNameField.getText(), Float.parseFloat(itemPriceField.getText()));
						r.addMenuItem(bp);
					} else {
						ArrayList<MenuItem> ingredients = new ArrayList<MenuItem>();
						String ingredientsString = itemIngredientsField.getText();
						ingredientsString = ingredientsString.replaceAll("\\s", "");
						String[] ingredient = ingredientsString.split(",");
						for(int i = 0; i < ingredient.length; i++) {
							ingredients.add(r.findItemByName(ingredient[i]));
						}
						CompositeProduct cp = new CompositeProduct(itemNameField.getText(), ingredients.get(0));
						for(int i = 1; i < ingredients.size(); i++) {
							cp.addMenuItem(ingredients.get(i));
						}
						r.addMenuItem(cp);
					}
					rs.serialize(r);
					break;
				case EDIT_ITEM:
					r.editMenuItem(r.findItemByName(itemNameField.getText()), itemEditNameField.getText(), Float.parseFloat(itemPriceField.getText()));
					rs.serialize(r);
					break;
				case DELETE_ITEM:
					MenuItem m = r.findItemByName(itemNameField.getText());
					r.menu.remove(m);
					rs.serialize(r);
					break;
				case VIEW_MENU:
					tablePanel.removeAll();
					JTable table = createTable();
					tablePanel.add(table.getTableHeader(), BorderLayout.PAGE_START);
					tablePanel.add(table, BorderLayout.CENTER);
					frame.revalidate();
					break;
			}
		}
	}
	
	private JTable createTable() {
		JTable ret;
		Object[][] entries = new Object[r.menu.size()][3];
		String[] collumnNames = {"Name", "Ingredients", "Price"};

		for(int i = 0; i < r.menu.size(); i++) {
			entries[i][0] = r.menu.get(i).getItemName();
			if(r.menu.get(i).getClass().getName().endsWith("CompositeProduct"))
				entries[i][1] = ((CompositeProduct) r.menu.get(i)).listIngredients();
			else
				entries[i][1] = "";
			entries[i][2] = r.menu.get(i).computePrice();
		}
		ret = new JTable(entries, collumnNames);
		return ret;
	}
	
	public static void main(String[] args) {
		AdministratorGraphicalUserInterface ui = new AdministratorGraphicalUserInterface();
		ui.initUI();
	}
}
