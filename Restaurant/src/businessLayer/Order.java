package businessLayer;

import java.time.LocalDate;

@SuppressWarnings("serial")
public class Order implements java.io.Serializable{
	
	private int orderId;
	private LocalDate date;
	private int table;
	
	public Order(int orderId,int table) {
		this.orderId = orderId;
		this.table = table;
		date = LocalDate.now();
	}
	
	public int getOrderId() {
		return orderId;
	}
	
	public LocalDate getDate() {
		return date;
	}
	
	public int getTable() {
		return table;
	}
	
	public int hashCode() {
		int key = 0;
		key = date.getDayOfYear() % 100 + table % 10;
		return key;
	}
	
	public boolean equals(Object o) {
		if(o.getClass().getName() == "Order") {
			if(((Order) o).getDate().equals(this.date) && ((Order) o).getOrderId() == this.orderId && ((Order) o).getTable() == this.table)
				return true;
		}
		return false;
	}
}
