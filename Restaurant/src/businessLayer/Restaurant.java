package businessLayer;

import java.util.Observable;
import java.util.ArrayList;
import java.util.HashMap;

@SuppressWarnings("serial")
public class Restaurant extends Observable implements java.io.Serializable{
	
	public HashMap<Order, ArrayList<MenuItem>> hashMap = new HashMap<Order, ArrayList<MenuItem>>();
	public ArrayList<MenuItem> menu = new ArrayList<MenuItem>();
	public ArrayList<Order> orders = new ArrayList<Order>();
	private int availableOrderId;
	
	public Restaurant() {
		availableOrderId = 1;
	}
	
	private Order createOrder(int table) {
		Order o = new Order(availableOrderId++, table);
		orders.add(o);
		return o;
	}
	
	public void addMenuItem(MenuItem item) {
		menu.add(item);
	}
	
	public void removeMenuItem(MenuItem item) {
		menu.remove(item);
	}
	
	public MenuItem findItemByName(String name) {
		MenuItem ret = null;
		for(MenuItem i : menu) {
			if(i.getItemName().equals(name))
				ret = i;
		}
		return ret;
	}
	
	public void editMenuItem(MenuItem item, String name, float price) {
		for(MenuItem m : menu) {
			if(m == item) {
				item.setItemName(name);
				if(m.getClass().getName().endsWith("BaseProduct")) {
					((BaseProduct) m).setPrice(price);
				}
			}
		}
	}
	
	public ArrayList<MenuItem> getItemsOfOrderId(int id) {
		Order order = null;
		for(Order o : orders) {
			if(o.getOrderId() == id)
				order = o;
		}
		return hashMap.get(order);
	}
	
	public void placeOrder(int table, ArrayList<MenuItem> itemList) {
		Order o = createOrder(table);
		hashMap.put(o, itemList);
		setChanged();
		notifyObservers();
	}
	
}
