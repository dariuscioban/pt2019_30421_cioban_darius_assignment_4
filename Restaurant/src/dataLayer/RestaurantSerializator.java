package dataLayer;

import java.io.*;
import businessLayer.Restaurant;

public class RestaurantSerializator {
	
	private String fileName;
	
	public RestaurantSerializator(String fileName) {
		this.fileName = fileName + ".ser";
	}
	
	public void serialize(Restaurant r) {
		try {
			FileOutputStream fileOut = new FileOutputStream(fileName);
			ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
			objectOut.writeObject(r);
			objectOut.close();
			fileOut.close();
		} catch (Exception e) {
		}
	}
	
	public Restaurant deserialize() {
		Restaurant r = null;
		try {
			FileInputStream fileIn = new FileInputStream(fileName);
			ObjectInputStream objectIn = new ObjectInputStream(fileIn);
			r = (Restaurant) objectIn.readObject();
			objectIn.close();
			fileIn.close();
		} catch (Exception e) {
		}
		return r;
	}
}
