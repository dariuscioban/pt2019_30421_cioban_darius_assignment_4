package presentationLayer;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import businessLayer.*;
import dataLayer.*;

import javax.swing.*;

public class WaiterGraphicalUserInterface {

	private static final int ADD_ORDER = 0;
	private static final int VIEW_ORDERS = 1;
	private static final int MAKE_BILL = 2;

	
	RestaurantSerializator rs = new RestaurantSerializator("restaurant");
	FileWriter fw = new FileWriter("Order");
	Restaurant r;
	
	private JFrame frame;
	JPanel tablePanel = new JPanel();
	JPanel rightPanel = new JPanel();
	JTextField orderTableField = new JTextField(10);
	JTextField orderItemsField = new JTextField(10);
	JTextField orderIdBill = new JTextField(10);
	
	ChefGraphicalUserInterface chefUi;
	
	public WaiterGraphicalUserInterface() {
		r = rs.deserialize();
	}
	
	public void initUI() {
		frame = new JFrame("Waiter");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(1400, 600);
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.X_AXIS));
		JPanel leftPanel = new JPanel();
		leftPanel.setLayout(new BoxLayout(leftPanel, BoxLayout.Y_AXIS));
		rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.Y_AXIS));
		tablePanel.setLayout(new BorderLayout());
		JPanel fieldPanel = new JPanel();
		JPanel buttonPanel = new JPanel();
		
		JButton addOrderButton = new JButton("Add");
		addOrderButton.addActionListener(new Clicker(ADD_ORDER));
		JButton viewOrdersButton = new JButton("View");
		viewOrdersButton.addActionListener(new Clicker(VIEW_ORDERS));
		JButton makeBillButton = new JButton("Bill");
		makeBillButton.addActionListener(new Clicker(MAKE_BILL));
		
		fieldPanel.add(new JLabel("Table: "));
		fieldPanel.add(orderTableField);
		fieldPanel.add(new JLabel("Items : "));
		fieldPanel.add(orderItemsField);
		fieldPanel.add(new JLabel("Order id : "));
		fieldPanel.add(orderIdBill);
		
		buttonPanel.add(addOrderButton);
		buttonPanel.add(viewOrdersButton);
		buttonPanel.add(makeBillButton);
		
		leftPanel.add(fieldPanel);
		leftPanel.add(new JSeparator());
		leftPanel.add(buttonPanel);
		rightPanel.add(tablePanel);
		mainPanel.add(leftPanel);
		mainPanel.add(new JSeparator());
		mainPanel.add(rightPanel);
		frame.setContentPane(mainPanel);
		frame.setVisible(true);
	}
	
	private class Clicker implements ActionListener {
		
		int mode;
		
		public Clicker(int mode) {
			this.mode = mode;
		}
		
		public void actionPerformed(ActionEvent arg0) {
			switch (mode) {
				case ADD_ORDER:
					r = rs.deserialize();
					r.addObserver(chefUi);
					int tableId = Integer.parseInt(orderTableField.getText());
					ArrayList<MenuItem> itemList = new ArrayList<MenuItem>();
					String itemsString = orderItemsField.getText();
					itemsString = itemsString.replaceAll("\\s", "");
					String[] item = itemsString.split(",");
					for(int i = 0; i < item.length; i++) {
						itemList.add(r.findItemByName(item[i]));
					}
					r.placeOrder(tableId, itemList);
					rs.serialize(r);
					break;
				case VIEW_ORDERS:
					tablePanel.removeAll();
					JTable table = createTable();
					tablePanel.add(table.getTableHeader(), BorderLayout.PAGE_START);
					tablePanel.add(table, BorderLayout.CENTER);
					frame.revalidate();
					break;
				case MAKE_BILL:
					makeBill(Integer.parseInt(orderIdBill.getText()));
					break;
			}
		}
	}
	
	private JTable createTable() {
		JTable ret;
		Object[][] entries = new Object[r.orders.size()][4];
		String[] collumnNames = {"Order Id", "Items", "Table", "Date"};

		for(int i = 0; i < r.orders.size(); i++) {
			entries[i][0] = r.orders.get(i).getOrderId();
			String itemsString = "";
			ArrayList<MenuItem> items = r.getItemsOfOrderId(r.orders.get(i).getOrderId());
			for(MenuItem m : items) {
				itemsString += m.getItemName() + ", ";
			}
			itemsString = itemsString.substring(0, itemsString.length() - 2);
			entries[i][1] = itemsString;
			entries[i][2] = r.orders.get(i).getTable();
			entries[i][3] = r.orders.get(i).getDate();
		}
		ret = new JTable(entries, collumnNames);
		return ret;
	}
	
	private void makeBill(int orderId) {
		String toPrint = "";
		Order order = null;
		float totalPrice = 0.0f;
		for(Order o : r.orders) {
			if(o.getOrderId() == orderId)
				order = o;
		}
		toPrint += "Table: " + Integer.toString(order.getTable()) + "\n";
		for(MenuItem m : r.getItemsOfOrderId(orderId)) {
			toPrint += "		" + m.getItemName() + ": " + Float.toString(m.computePrice()) + "\n";
			totalPrice += m.computePrice();
		}
		toPrint += "Total price: " + Float.toString(totalPrice) + "$";
		fw.write(toPrint);
	}
	
	public static void main(String[] args) {
		WaiterGraphicalUserInterface ui = new WaiterGraphicalUserInterface();
		ui.initUI();
		ui.chefUi = new ChefGraphicalUserInterface();
		ui.r.addObserver(ui.chefUi);
	}
}
