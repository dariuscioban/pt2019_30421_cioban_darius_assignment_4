package presentationLayer;

import java.time.LocalTime;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

public class ChefGraphicalUserInterface implements Observer{

	JTextArea logArea;
	JFrame frame;
	
	public ChefGraphicalUserInterface() {
		frame = new JFrame("Chef");
		logArea = new JTextArea(15, 25);
		logArea.setEditable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(500, 300);
		JPanel mainPanel = new JPanel();
		JScrollPane scroll = new JScrollPane(logArea);
	    scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		mainPanel.add(scroll);
		frame.setContentPane(mainPanel);
		frame.setVisible(true);
	}
	
	public void update(Observable arg0, Object arg1) {
		System.out.println("Ok");
		logArea.setText(logArea.getText() + "Order placed at " + LocalTime.now().toString() + "\n");
		frame.revalidate();
	}
}
