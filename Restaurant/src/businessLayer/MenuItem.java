package businessLayer;

@SuppressWarnings("serial")
public abstract class MenuItem implements java.io.Serializable{
	private String itemName;
	
	public MenuItem(String itemName) {
		this.itemName = itemName;
	}
	
	public String getItemName() {
		return itemName;
	}
	
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	
	public abstract float computePrice();
}
